// import { WealthscopeSdk } from "../lib/wealthscope-partner-sdk/src/index";

var addon, addonOptions;
$(function() {
  addon = new Addon();
  addon
    .on("init", function(options) {
      // Dashboard is ready and is signaling to the add-on that it should
      // render using the passed in options (filters, language, etc.)
      addonOptions = options;

      // Setup Wealthscope
      // ws = new WealthscopeSdk();
      // ws.render(document.getElementById("result")); // Render Wealthscope
    })
    .on("update", function(options) {
      // Filters have been updated and Dashboard is passing in updated options
      addonOptions = _.extend(addonOptions, options);
    });
});
